import './index.scss';
import withAuth from '../hoc/withAuth';
import ProfilePageDisplayTranslations from './ProfilePageDisplayTranslations';
import ProfilePageClearTranslations from './ProfilePageClearTranslations';
import { useState } from 'react';
/*
  Parent component for profile page
*/
const ProfilePage = () => {
  // Listens to button click to rerender the most recent translations after deleted
  const [changed, setChanged] = useState("");
  return (
    <div className="profile-page">
      <h1>ProfilePage</h1>
      <ProfilePageDisplayTranslations changed={changed} />
      <ProfilePageClearTranslations setChanged={setChanged} />
    </div>
  );
};

export default withAuth(ProfilePage);