import './index.scss';
import { useEffect } from 'react';

const TranslationPageOutput = ({ translation }) => {

  /*
    Rerender for every translation.
  */
  useEffect(() => { }, [translation]);

  /*
    Replace every space with _ for space image.
  */
  translation = translation.toLowerCase();
  translation = translation.replaceAll(' ', '_');

  /*
    Convert the tranlation string to an array with char and map each char to an img stored locally.
    Render the hand sign images to represent the translation string.
  */

  const charArr = Array.from(translation);
  const charToPic = charArr.map((i, index) => (
    <img
      className="sign-img"
      key={index}
      src={`./individial_signs/${i}.png`}
      alt='sign'
    ></img>
  ));
  return (
    <div className="translate-output">
      <h1>TranslationPage Output</h1>
      <div className="translate-output-images">{charToPic}</div>
    </div>
  );
};

export default TranslationPageOutput;