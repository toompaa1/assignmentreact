import "./index.scss";

/*
  Gets the login method as props from parent.
*/
const StartPageButton = ({ login }) => {
  return <button onClick={login}>Login</button>;
};

export default StartPageButton;