import "./index.scss";

/*
  Get the value from input and send it back as props to parent.
*/
const StartPageInput = ({ getUserEmail }) => {
  const getInputValue = (e) => {
    getUserEmail(e.target.value);
  };
  return (
    <div>
      <input
        onChange={getInputValue}
        className="input-name"
        type="email"
        id="input-name"
        placeholder="What´s your email?"
      />
    </div>
  );
};

export default StartPageInput;