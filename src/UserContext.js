import { createContext, useState } from "react";

/*
  Here we create the UserContext.
*/
export const UserContext = createContext(null);

/*
  Here we create the stat for the provider that will make sure every components has access to.
*/
export const UserProvider = ({ children }) => {
  const [contextEmail, setContextEmail] = useState(null);
  return (
    <UserContext.Provider value={{ contextEmail, setContextEmail }}>
      {children}
    </UserContext.Provider>
  );
};
