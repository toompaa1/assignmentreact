import "./index.scss";
import { Link } from "react-router-dom";
import NavbarProfile from "./navbarProfile";

/*
  Parent component for navbar
*/

const Navbar = () => {
  return (
    <div>
      <nav className="navbar">
        <div className="navbar-title">
          <Link className="navbar-link" to="/">
            <h5>Lost in Translation</h5>
          </Link>
        </div>
        <NavbarProfile />
      </nav>
    </div>
  );
};

export default Navbar;