import { getByEmail } from '../../util/api';
import './index.scss';
import { useEffect, useState } from 'react';

const ProfilePageDisplayTranslations = ({ changed }) => {
  const [recentTranslations, setRecentTranslations] = useState([]);
  useEffect(() => {
    getAndDisplay()
  }, [changed]);

  /*
    Method to get current userObj => get translations from userObj, get 10 last translations and display.
    If the translations is deleted dont display anything.
  */
  const getAndDisplay = async () => {
    const email = localStorage.getItem('email');
    const user = await getByEmail(email);
    let translations = user[0].translations
    let tempTranslations = []
    if (translations.length > 10) {
      for (let index = translations.length - 1; index >= translations.length - 10; index--) {
        if (translations[index] !== 'Deleted') {
          tempTranslations.push(translations[index])
        }
      }
      translations = tempTranslations
    } else {
      translations.forEach(s => {
        if (s !== 'Deleted') {
          tempTranslations.push(s)
        }
      });
      translations = tempTranslations
    }
    await setRecentTranslations(translations);
  }

  /*
    Prints out every image and translations as JSX.
  */
  let image;
  for (let i = 0; i < recentTranslations.length; i++) {
    let temp = recentTranslations[i]
    temp = temp.toLowerCase();
    temp = temp.replaceAll(' ', '_')
    let charArr = Array.from(temp)
    image = charArr.map((p, index) => <img className="sign-image" key={index} alt='signs' src={`./individial_signs/${p}.png`}></img>)
  }
  const listOfTranslations = recentTranslations.map((t, index) => (
    <li className="profile-page-li" key={index}><p className="profile-page-text">{t}</p>{image}</li>
  ));
  return (
    <div>
      <ol>{recentTranslations ? listOfTranslations : null}</ol>
    </div>
  );
};

export default ProfilePageDisplayTranslations;