import './index.scss';
import { getByEmail, deleteTranslations } from '../../util/api';

const ProfilePageClearTranslations = ({ setChanged }) => {

    /*
        Method to clear and change translations in db. We get current user => get userObj from api.
        Then we get the user translations and foreach of the translations should be "Deleted" in db.
        Send curent transations with the deletedTranslations method from api.
    */
    const clearTranslations = async () => {
        const email = localStorage.getItem('email');
        const user = await getByEmail(email);
        let currentTranslations = user[0].translations
        const updatedWithDeletedTranslations = currentTranslations.map(t => "Deleted")
        await deleteTranslations(email, updatedWithDeletedTranslations)
        setChanged("update");
    }
    return (
        <div>
            <button onClick={clearTranslations}> Clear translations </button>
        </div>
    );
};

export default ProfilePageClearTranslations;