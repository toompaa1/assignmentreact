import "./App.scss";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import StartPage from "./components/startpage/StartPage";
import TranslationPage from "./components/translationpage/TranslationPage";
import ProfilePage from "./components/profilepage/ProfilePage";
import Navbar from "./components/navbar/navbar";
import { UserProvider } from "./UserContext";

function App() {
  return (
    <Router>
      <UserProvider>
        <div className="App">
          <header>
            <Navbar />
          </header>
          <main>
            <Switch>
              <Route exact path="/" component={StartPage} />
              <Route path="/translation" component={TranslationPage} />
              <Route path="/profile" component={ProfilePage} />
            </Switch>
          </main>
          <footer></footer>
        </div>
      </UserProvider>
    </Router>
  );
}

export default App;
