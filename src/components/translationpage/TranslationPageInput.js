import './index.scss';
import { updateUser } from '../../util/api';
import { useContext, useState } from 'react';
import { UserContext } from '../../UserContext';

const TranslationInput = ({ getTranslation }) => {
  const { contextEmail } = useContext(UserContext);
  const [previousInput, setPreviousInput] = useState("");

  /*
    Submit function will forward the input result to the output page as hand signs
  */
  const submit = async (e) => {
    e.preventDefault();
    // Validates if the string only obtain english letter , length is greater then 0 and lower then 40. 
    if (/^[a-zA-Z/ /]*$/.test(e.target[0].value) && e.target[0].value.length < 40 && e.target[0].value.length > 0 && previousInput !== e.target[0].value) {
      // Updates the state and database with the new translation
      getTranslation(e.target[0].value);
      setPreviousInput(e.target[0].value)
      await updateUser(contextEmail, e.target[0].value);
    } else {
      // Prompt the user with correct error based on invalid input
      if (e.target[0].value <= 0) {
        alert("Field cannot be empty")
      } else if (previousInput === e.target[0].value) {
        alert(`${previousInput} was just translated !`)
      } else if (e.target[0].value.length > 40) {
        alert("Max length is 40 characters")
      } else if (!(/^[a-zA-Z/ /]*$/.test(e.target[0].value.length))) {
        alert("Only english letters are supported")
      }
      // Reset state and field when incorrect input was given
      getTranslation('');
      e.target[0].value = ''
    }
  };

  return (
    <div>
      <form onSubmit={submit}>
        <input
          className="input-translation"
          type="text"
          id="input-translation"
          placeholder="What would you like to translate?"
        />
        <button className="translate-button">Translate</button>
      </form>
    </div>
  );
};

export default TranslationInput;