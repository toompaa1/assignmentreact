//const BASE_URL = 'http://localhost:3001/users/';
const BASE_URL = 'https://fake-server-react-app-db.herokuapp.com/users/';
//const GET_USER_URL = 'http://localhost:3001/users?email=';
const GET_USER_URL = 'https://fake-server-react-app-db.herokuapp.com/users?email=';

/*
  GET userObj with email.
*/
export async function getByEmail(email) {
  try {
    let response = await fetch(GET_USER_URL + email);
    let data = response.json();
    return await data;
  } catch (e) {
    console.log(e, "Failed to get user!");
  }
}

/*
  POST userObj with email.
*/
export async function addNewUser(email) {
  let user = {
    email: email,
    translations: [],
  };
  try {
    await fetch(BASE_URL, {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
  } catch (error) {
    console.log(error, 'Failed to add new user!');
  }
}

/*
  PUT userObj with email and new translations.
*/
export async function updateUser(email, translation) {
  let user = await getUserByEmail(email);
  let translations = user[0].translations;
  translations.push(translation);
  let updateUser = {
    email: email,
    translations: translations,
  };
  try {
    return await fetch(`${BASE_URL}/${user[0].id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updateUser),
    })
  } catch (error) {
    console.log(error, 'Failed to insert translation!')
  }
}

/*
  PUT userObj with email and deleted translations.
*/
export async function deleteTranslations(email, translation) {
  let user = await getUserByEmail(email);
  let updateUser = {
    email: email,
    translations: translation,
  };
  try {
    return await fetch(`${BASE_URL}/${user[0].id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updateUser),
    })
  } catch (error) {
    console.log(error, 'Failed to delete translations!')
  }
}

/*
  GET userObj with email.
*/
async function getUserByEmail(email) {
  try {
    let response = await fetch(GET_USER_URL + email);
    let data = response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to get user!");
  }
}