import './index.scss';
import withAuth from '../hoc/withAuth';
import TranslationInput from './TranslationPageInput';
import TranslationPageOutput from './TranslationPageOutput';
import { useState } from 'react';

/*
  Parent component for translation page
*/
const TranslationPage = () => {
  const [translation, setTranslation] = useState('');
  const getTranslation = (translation) => {
    setTranslation(translation);
  };
  return (
    <div className="translation-page">
      <h1>TranslationPage</h1>
      <TranslationInput getTranslation={getTranslation} />
      <TranslationPageOutput translation={translation} />
    </div>
  );
};

export default withAuth(TranslationPage);