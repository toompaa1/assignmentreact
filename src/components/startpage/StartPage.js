import { useEffect, useState, useContext } from "react";
import "./index.scss";
import StartPageInput from "./StartPageInput";
import StartPageButton from "./StartPageButton";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../UserContext";
import { addNewUser, getByEmail } from "../../util/api";

/*
  Parent component for start page
*/

const StartPage = () => {
  const { setContextEmail } = useContext(UserContext);
  const [email, setUserEmail] = useState("");
  const history = useHistory();

  /*
    On component create, check if there is a user => if there is a user take user to translation page.
  */
  useEffect(() => {
    if (localStorage.hasOwnProperty("email")) {
      history.push("/translation");
    }
  }, []);

  /*
    Method to login the user. First we check if there is a user in the db. Then check if the user type in valid email.
    Then set user to localstorage and add user to db with api. 
    Take user to translation page.
  */
  const login = async () => {
    let checkUser = await getByEmail(email)
    if (email === "") {
      alert("Type in valid email");
    } else if (checkUser.length > 0) {
      localStorage.setItem("email", email);
      setContextEmail(email);
      history.push("/translation");
    }
    else {
      localStorage.setItem("email", email);
      setContextEmail(email);
      history.push("/translation");
      addNewUser(email);
    }
  };

  /*
    Get the value from input field.
    And setUserEmail with input value. 
  */
  const getUserEmail = (email) => {
    setUserEmail(email);
  };
  return (
    <div className="startpage">
      <StartPageInput getUserEmail={getUserEmail} />
      <StartPageButton login={login} />
    </div>
  );
};

export default StartPage;