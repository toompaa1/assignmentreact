import { Redirect } from "react-router-dom";

/*
  Auth to check if there is a user in localstorage, if there is a user..
  redirect to right component else to startpage.
*/
function withAuth(Component) {
  return function () {
    if (localStorage.hasOwnProperty("email")) {
      return <Component />;
    } else {
      return <Redirect to="/" />;
    }
  };
}

export default withAuth;
