import "./index.scss";
import { Link, useHistory } from "react-router-dom";
import { useContext, useEffect } from "react";
import { UserContext } from "../../UserContext";

const NavbarProfile = () => {
  const { contextEmail, setContextEmail } = useContext(UserContext);
  const history = useHistory();

  /*
    On component create => get current user. And listen on contextEmail and rerender.
  */
  useEffect(() => {
    setContextEmail(localStorage.getItem('email'))
  }, [contextEmail])

  /*
    Clear localstorage and set the userContextEmail to empty and then send user to startpage.
  */
  const logout = () => {
    localStorage.clear()
    setContextEmail("");
    history.push("/")
  }
  return (
    <div className="navbar-profile">
      {contextEmail ?
        <div className="navbar-profile-container">
          <Link to="/profile" className="navbar-profile-link" >
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-person-circle" viewBox="0 0 16 16">
              <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
              <path fillRule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
            </svg><h5 className="navbar-h5">{contextEmail}</h5>
          </Link >
          <button onClick={logout}>Logout</button>
        </div>
        : null}
    </div >

  );
};

export default NavbarProfile;